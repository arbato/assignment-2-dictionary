%include "colon.inc"
section .data


colon "r a", fifth_word
db "ok ok", 0 

colon "supercalifragilisticexpialidocious", fourth_word
db "we dont know", 0 

colon "prescient", third_word
db "having or showing knowledge of events before they take place.", 0

colon "effervescent", second_word
db "when a liquid is bubbly or fizzy", 0 

colon "cheetah", first_word
db "an animal with spots", 0 

colon "dog", zero_word
db "an animal that barks", 0 