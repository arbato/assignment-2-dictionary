%include "lib.inc"
%define keysize 8
global find_word
section .text

; find_word traverses the linked dictionary and returns KEY (not value) if found, else zero
; rdi - pointer to sought for word 
; rsi - pointer to first element in dictionary


find_word: 
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
    .loop:
        test r13, r13       ; check if end of list has been reached
        jz .exit      
                            ; rdi(r12) is the word we are looking for
        
        mov rdi, r12
        mov  rsi, [r13+ keysize]

        call string_equals  ; returns 1 if equal, else 0

        test rax, rax       
        jnz .found          ; if rax !=0, word was found

        mov r13, [r13]      ; else - get the address of the next element
        jmp .loop

    .found:
        mov rax, r13        ; ret address of key
        jmp .done
        
    .exit:
        xor rax, rax        ; ret 0
        jmp .done
    .done:
    	pop r13
    	pop r12
    	ret

        