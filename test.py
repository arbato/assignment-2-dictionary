from subprocess import PIPE, Popen
from unittest import TestCase

inputs = ["", "dog", "cheetah", "asjdklfALSKDJFKALSLDKFJKALSalskdjaksldfldksjaksldfldksjaksldfdlksjaksldfdlksjaskdlf;ldksjaksldfldksjaksdlfdlksjaskldfldksjasjasdfghjkasdfghjkasdfghjkasdfghjkasdfghjkasdfjfkslalskdjfkdslalskdjfkdlsalskjdfkdlsalksdjfkdlslaksjdkfldslaksjdkldklfALSKDJFKALSLDKFJKALSalskdjfkslalskdjfkdslalskdjfkdlsalskjdfkdlsalksdjfkdlslaksjdkfldslaksjdklfdklfALSKDJFKALSLDKFJKALSalskdjfkslalskdjfkdslalskdjfkdlsalskjdfkdlsalksdjfkdlslaksjdkfldslaksjdklffslkajskdlfld"]
exp_out = ["", "an animal that barks", "an animal with spots", ""]
exp_err = ["Key not found in dictionary", "", "", "Invalid input"]

passed=True

for i in range(len(inputs)):
    process = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate(input=inputs[i].encode())
    out=out.decode()
    err=err.decode()
    TestCase().assertEqual(out.strip(), exp_out[i])
    if (out.strip() != exp_out[i] or err.strip() != exp_err[i]):
        passed=False

if (passed):
    print("All tests passed :0 :)")

